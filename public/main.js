miro.onReady(() => {
  miro.initialize({
    extensionPoints: {
        getWidgetMenuItems: (widgets) => {
            return Promise.resolve([{
                tooltip: 'Spin',
                svgIcon: Config.spinIcon,
                onClick: spinCards(widgets)
            },
            {
                tooltip: 'Shuffle',
                svgIcon: Config.shuffleIcon,
                onClick: shuffleCards(widgets)
            },
            {
                tooltip: 'Arrange',
                svgIcon: Config.arrangeIcon,
                onClick: arrangeCards(widgets)
            },
/*
            {
                tooltip: 'Flip',
                svgIcon: Config.flipIcon,
                onClick: flipCards(widgets)
            }
*/            

        ])
        }
    }
});
})
function shuffleCards(widgets) {
    return async (widgets) => {
        console.log('onClick Shuffle', widgets);

        // Get selected widgets
        const selectedWidgets = await miro.board.selection.get();

        var shuffleSteps = Math.floor(selectedWidgets.length * 2.5);
        var lastWidget;
        for (i = 0; i < shuffleSteps; i++) {
            var randomIndex = Math.floor(Math.random() * selectedWidgets.length);
            var randomWidget = selectedWidgets[randomIndex];
            lastWidget = randomWidget;
            await miro.board.widgets.bringForward(randomWidget.id);
        }

        await miro.board.widgets.__blinkWidget(lastWidget.id);
        console.log(lastWidget);

        await miro.showNotification('Shuffling done.');
    };
}

function spinCards(widgets) {
    return async (widgets) => {
        console.log('onClick Spin', widgets);

        // Get selected widgets
        const selectedWidgets = await miro.board.selection.get();

        for (i = 0; i < selectedWidgets.length; i++) {
            var randomAngel = Math.floor(Math.random() * 36) + 1;
            randomAngel = (randomAngel * 10) + 360;
            await miro.board.widgets.transformDelta(selectedWidgets[i].id, 0, 0, randomAngel);
            await miro.board.widgets.__blinkWidget(selectedWidgets[i].id);
        }

        await miro.showNotification('Spinning done.');
    };
}
function arrangeCards(widgets) {
    return async (widgets) => {
        console.log('onClick Arrange', widgets);

        // Get selected widgets
        const selectedWidgets = await miro.board.selection.get();
        const angleDelta = (140 / (selectedWidgets.length));

        const baseWidget = selectedWidgets[0];
        //we gonna stretch the widgets linear on x, expecting that all the widgets have the same width
        const deltaX = (baseWidget.bounds.width) / selectedWidgets.length;
        const radiusY  = baseWidget.bounds.height * 0.2;

        for (i = 0; i < selectedWidgets.length; i++)
        {
            const currentWidget = selectedWidgets[i];
            const angle = angleDelta * i - 70;
            const x = i * deltaX;
            const y = Math.sin(angle) * radiusY;

            console.log({
                x: x,
                y: y,
                angle: angle
            });

            await miro.board.widgets.transformDelta(currentWidget.id, x, y, angle);
        }
        
        await miro.showNotification('Arrange done.');
    };
}

const cards = [];
function flipCards(widgets) {
    return async (widgets) => {
        const appId = await miro.getClientId();

        console.log('onClick Flip', widgets);

        // Get selected widgets
        const selectedWidgets = await miro.board.selection.get();

        await miro.board.widgets.update([
            {
                id: selectedWidgets[0].id,
                url: selectedWidgets[0].metadata[appId].otherImageUrl,
                "metadata[appId].otherImageUrl": selectedWidgets[0].url
            }
        ]);
        cards.push(selectedWidgets);
        console.log(cards);
    };
}